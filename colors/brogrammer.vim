" brogrammer.vim - Vim color scheme
" ----------------------------------------------------------
" Version:	1.0
" Author:	Byron Mansfield (http://byronmansfield.com/)
" License:	Creative Commons Attribution-NonCommercial
" ----------------------------------------------------------

" Setup ----------------------------------------------------
set background=dark
hi clear

if exists("syntax_on")
	syntax reset
endif

set t_Co=256

" Declare theme name
let g:colors_name="brogrammer"

" Colors --------------------------------------------------
let s:white=            { "cterm": "15",  "gui": "#f7f3ff" }
let s:black=            { "cterm": "16" , "gui": "#000000" }
let s:almostWhite=      { "cterm": "231", "gui": "#ecf0f1" }
let s:almostBlack=      { "cterm": "234", "gui": "#1a1a1a" }
let s:darkGrey=         { "cterm": "238", "gui": "#444444" }
let s:lightGrey=        { "cterm": "244", "gui": "#838586" }
let s:middleLightGrey=  { "cterm": "241", "gui": "#606060" }
let s:middleDarkGrey=   { "cterm": "236", "gui": "#2f2f2f" }
let s:middleDarkerGrey= { "cterm": "240", "gui": "#575858" }

let s:green=            { "cterm": "2"  , "gui": "#009900" }
let s:brightGreen=      { "cterm": "40" , "gui": "#00df00" }
let s:lightGreen=       { "cterm": "41" , "gui": "#1dce6d" }
let s:darkGreen=        { "cterm": "22" , "gui": "#30312a" }
let s:orangeGreen=      { "cterm": "64" , "gui": "#44800a" }
let s:blueGreen=        { "cterm": "23" , "gui": "#1d3251" }

let s:red=              { "cterm": "1"  , "gui": "#ff2222" }
let s:brightRed=        { "cterm": "196", "gui": "#e14d42" }
let s:darkRed=          { "cterm": "88" , "gui": "#880505" }

let s:blue=             { "cterm": "39" , "gui": "#6c71c4" }
let s:darkBlue=         { "cterm": "23" , "gui": "#1d3251" }

let s:purple=           { "cterm": "62" , "gui": "#6b6ec7" }
let s:faintPurple=      { "cterm": "68" , "gui": "#3498db" }

let s:pink=             { "cterm": "224", "gui": "#f7e8e2" }
let s:darkPink=         { "cterm": "167", "gui": "#ea4b35" }

let s:yellow=           { "cterm": "220", "gui": "#f2c600" }
let s:orange=           { "cterm": "172", "gui": "#e87e0a" }
" let s:blue=             { "cterm": "32" , "gui": "#2d96de" }

let s:norm=s:almostWhite
let s:comment=s:middleLightGrey
let s:dimmed=s:middleDarkGrey
let s:subtle=s:darkGrey
let s:faint=s:almostBlack

" Utilility Function ---------------------------------------
function! s:h(group, style)
	execute "highlight" a:group
		\ "guifg="   (has_key(a:style, "fg")    ? a:style.fg.gui   : "NONE")
		\ "guibg="   (has_key(a:style, "bg")    ? a:style.bg.gui   : "NONE")
		\ "guisp="   (has_key(a:style, "sp")    ? a:style.sp.gui   : "NONE")
		\ "gui="     (has_key(a:style, "gui")   ? a:style.gui      : "NONE")
		\ "ctermfg=" (has_key(a:style, "fg")    ? a:style.fg.cterm : "NONE")
		\ "ctermbg=" (has_key(a:style, "bg")    ? a:style.bg.cterm : "NONE")
		\ "cterm="   (has_key(a:style, "cterm") ? a:style.cterm    : "NONE")
endfunction

" Highlights
call s:h("CursorLine",  {                    "bg": s:dimmed                                                                     })
call s:h("MatchParen",  { "fg": s:darkPink,                     "cterm": "underline",   "gui": "underline"                      })
call s:h("Pmenu",       { "fg": s:lightGreen                                                                                    })
call s:h("PmenuThumb",  {                    "bg": s:norm                                                                       })
call s:h("PmenuSBar",   {                    "bg": s:subtle                                                                     })
call s:h("PmenuSel",    {                    "bg": s:subtle                                                                     })
call s:h("SpellBad",    { "fg": s:darkPink,                     "cterm": "bold,italic", "gui": "bold,italic", "sp": s:darkPink  })
call s:h("SpellCap",    {                                       "cterm": "underline",   "gui": "underline",   "sp": s:blue      })
call s:h("SpellRare",   {                                       "cterm": "underline",   "gui": "underline",   "sp": s:green     })
call s:h("SpellLocal",  {                                       "cterm": "underline",   "gui": "underline",   "sp": s:orange    })
hi! link CursorColumn	  CursorLine
hi! link ColorColumn	  CursorLine

" Use background for cterm Spell*, which does not support undercurl
execute "hi! SpellCap   ctermbg=" s:blue.cterm
execute "hi! SpellRare  ctermbg=" s:green.cterm
execute "hi! SpellLocal ctermbg=" s:faint.cterm

" Highlights - UI ---------------------------------------------
call s:h("Normal",       { "fg": s:norm,        "bg": s:faint                                       })
call s:h("NonText",      { "fg": s:darkGreen,   "bg": s:faint                                       })
call s:h("LineNr",       { "fg": s:lightGrey,   "bg": s:dimmed                                      })
call s:h("Cursor",       { "fg": s:faint,       "bg": s:norm                                        })
call s:h("Visual",       { "fg": s:faint,       "bg": s:yellow,      "cterm": "bold", "gui": "bold" })
call s:h("IncSearch",    { "fg": s:black,       "bg": s:orange,      "cterm": "bold", "gui": "bold" })
call s:h("Search",       { "fg": s:faint,       "bg": s:yellow,      "cterm": "bold", "gui": "bold" })
call s:h("StatusLine",   { "fg": s:norm,        "bg": s:comment,     "cterm": "bold", "gui": "bold" })
call s:h("StatusLineNC", { "fg": s:norm,        "bg": s:comment                                     })
call s:h("SignColumn",   { "fg": s:lightGrey,   "bg": s:dimmed                                      })
call s:h("VertSplit",    { "fg": s:comment,     "bg": s:comment                                     })
call s:h("TabLine",      { "fg": s:dimmed,      "bg": s:faint                                       })
call s:h("TabLineSel",   {                                           "cterm": "bold", "gui": "bold" })
call s:h("Folded",       { "fg": s:comment,     "bg": s:faint                                       })
call s:h("SpecialKey",   { "fg": s:darkGreen,   "bg": s:dimmed                                      })
call s:h("Directory",    { "fg": s:darkPink                                                         })
call s:h("Title",        { "fg": s:norm,                             "cterm": "bold", "gui": "bold" })
call s:h("ErrorMsg",     { "fg": s:white,       "bg": s:darkPink                                    })
call s:h("DiffAdd",      { "fg": s:norm,        "bg": s:orangeGreen, "cterm": "bold", "gui": "bold" })
call s:h("DiffDelete",   { "fg": s:darkRed                                                          })
call s:h("DiffChange",   { "fg": s:norm,        "bg": s:blueGreen                                   })
call s:h("DiffText",     { "fg": s:norm,        "bg": s:brightGreen, "cterm": "bold", "gui": "bold" })
call s:h("User1",        { "fg": s:black,       "bg": s:green                                       })
call s:h("User2",        { "fg": s:black,       "bg": s:red                                         })
call s:h("User3",        { "fg": s:black,       "bg": s:blue                                        })
hi! link WildMenu	IncSearch
hi! link FoldColumn	SignColumn
hi! link WarningMsg	ErrorMsg
hi! link MoreMsg	Title
hi! link Question	MoreMsg
hi! link ModeMsg	MoreMsg
hi! link TabLineFill	StatusLineNC

" Highlights - Generic Syntax ---------------------------------
call s:h("Delimiter",   { "fg": s:dimmed                                                                    })
call s:h("Comment",     { "fg": s:comment,                                     "gui": "italic"              })
call s:h("Underlined",  {                      "cterm": "underline",           "gui": "underline"           })
call s:h("Type",        { "fg": s:lightGreen                                                                })
call s:h("Boolean",     { "fg": s:purple                                                                    })
call s:h("Character",   { "fg": s:purple                                                                    })
call s:h("String",      { "fg": s:yellow                                                                    })
call s:h("Keyword",     { "fg": s:darkPink,    "cterm": "bold",                "gui": "bold"                })
call s:h("Float",       { "fg": s:darkPink                                                                  })
call s:h("Todo",        { "fg": s:comment,     "cterm": "bold,italic,inverse", "gui": "bold,italic,inverse" })
call s:h("Label",       { "fg": s:yellow                                                                    })
call s:h("Function",    { "fg": s:green                                                                     })
call s:h("Identifier",  { "fg": s:faintPurple                                                               })
call s:h("Statement",   { "fg": s:blue,        "cterm": "bold",                "gui": "bold"                })
call s:h("Conditional", { "fg": s:darkPink,    "cterm": "bold",                "gui": "bold"                })
call s:h("Define",      { "fg": s:darkPink,    "cterm": "bold",                "gui": "bold"                })
call s:h("Operator",    { "fg": s:darkPink,    "cterm": "bold",                "gui": "bold"                })
call s:h("PreProc",     { "fg": s:darkPink,    "cterm": "bold",                "gui": "bold"                })
call s:h("StorageClass",{ "fg": s:faintPurple                                                               })
call s:h("Structure",   { "fg": s:faintPurple, "cterm": "bold",                "gui": "bold"                })
call s:h("Tag",         { "fg": s:lightGreen                                                                })
hi! link Constant	Directory
hi! link Number		Constant
hi! link Special	Constant
hi! link Error		ErrorMsg

" Shell ----------------------------------------------------
call s:h("shSet", 				{ "fg": s:blue 				})
call s:h("shSetOption", 	{ "fg": s:darkPink 		})
call s:h("shVariable", 		{ "fg": s:faintPurple })
call s:h("shVarAssign", 	{ "fg": s:norm 				})
call s:h("shStatement", 	{ "fg": s:blue 				})
call s:h("shHereString", 	{ "fg": s:dimmed 			})
call s:h("shQuote", 			{ "fg": s:dimmed 			})
call s:h("shSingleQuote",	{ "fg": s:yellow 			})
call s:h("shDoubleQuote",	{ "fg": s:dimmed 			})
call s:h("shDerefSimple",	{ "fg": s:darkPink 		})
call s:h("shDerefVar",		{ "fg": s:darkPink 		})
hi! link shOperator	Delimiter
hi! link shCaseBar	Delimiter

" Python ---------------------------------------------------
" keywords
syn keyword pythonStatement 		break continue del
syn keyword pythonStatement 		exec
syn keyword pythonStatement 		pass raise
syn keyword pythonStatement 		global nonlocal assert
syn keyword pythonStatement 		yield
syn keyword pythonStatement 		with as
syn keyword pythonStatement 		print
syn keyword pythonStatement 		async await
syn keyword pythonSuper 				super
syn keyword pythonReturn 				return
syn keyword pythonRepeat        for while
syn keyword pythonConditional   if elif else
syn keyword pythonInclude       import from
syn keyword pythonException     try except finally
syn keyword pythonOperator      and in is not or

syn keyword pythonClassStmt class nextgroup=pythonClass skipwhite
syn match   pythonClass "\h\w*" display contained

" syn clear 	pythonAttribute
syn match   pythonAttribute /\.\h\w*/hs=s+1
    \ contains=ALLBUTythonBuiltinythonFunctionythonClassythonAsync
    \ transparent

" colors
call s:h("pythonInclude",     { "fg": s:darkPink })
call s:h("pythonConditional", { "fg": s:darkPink })
call s:h("pythonStatement",   { "fg": s:blue     })
call s:h("pythonNumber",   		{ "fg": s:blue     })
call s:h("pythonException",   { "fg": s:darkPink })
call s:h("pythonSuper",       { "fg": s:purple 	 })
call s:h("pythonReturn",      { "fg": s:darkPink })
call s:h("pythonRepeat",      { "fg": s:darkPink })
call s:h("pythonOperator",    { "fg": s:darkPink })
call s:h("pythonFunction",    { "fg": s:green    })
call s:h("pythonBuiltin",     { "fg": s:darkPink })
call s:h("pythonClass",       { "fg": s:darkPink })
call s:h("pythonClassStmt",   { "fg": s:blue 	 	 })

" Ruby -----------------------------------------------------
call s:h("rubyClass", { "fg": s:darkPink, "cterm": "bold", "gui": "bold" })
" hi rubyClass ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
" hi rubyFunction ctermfg=41 ctermbg=NONE cterm=NONE guifg=#2ecc71 guibg=NONE gui=NONE
" hi rubyInterpolationDelimiter ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
" hi rubySymbol ctermfg=62 ctermbg=NONE cterm=NONE guifg=#6c71c4 guibg=NONE gui=NONE
" hi rubyConstant ctermfg=68 ctermbg=NONE cterm=bold guifg=#3498db guibg=NONE gui=bold
" hi rubyStringDelimiter ctermfg=220 ctermbg=NONE cterm=NONE guifg=#f1c40f guibg=NONE gui=NONE
" hi rubyBlockParameter ctermfg=172 ctermbg=NONE cterm=NONE guifg=#e67e22 guibg=NONE gui=NONE
" hi rubyInstanceVariable ctermfg=172 ctermbg=NONE cterm=NONE guifg=#e67e22 guibg=NONE gui=NONE
" hi rubyInclude ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
" hi rubyGlobalVariable ctermfg=172 ctermbg=NONE cterm=NONE guifg=#e67e22 guibg=NONE gui=NONE
" hi rubyRegexp ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
" hi rubyRegexpDelimiter ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
" hi rubyEscape ctermfg=62 ctermbg=NONE cterm=NONE guifg=#6c71c4 guibg=NONE gui=NONE
" hi rubyControl ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
" hi rubyClassVariable ctermfg=172 ctermbg=NONE cterm=NONE guifg=#e67e22 guibg=NONE gui=NONE
" hi rubyOperator ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
" hi rubyException ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
" hi rubyPseudoVariable ctermfg=172 ctermbg=NONE cterm=NONE guifg=#e67e22 guibg=NONE gui=NONE
" hi rubyRailsUserClass ctermfg=68 ctermbg=NONE cterm=bold guifg=#3498db guibg=NONE gui=bold
" hi rubyRailsARAssociationMethod ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
" hi rubyRailsARMethod ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
" hi rubyRailsRenderMethod ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
" hi rubyRailsMethod ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
" hi erubyDelimiter ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
" hi erubyComment ctermfg=241 ctermbg=NONE cterm=NONE guifg=#606060 guibg=NONE gui=italic
" hi erubyRailsMethod ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE

" Highlights - HTML -------------------------------------------
hi! link htmlLink	Underlined
hi! link htmlTag	Type
hi! link htmlEndTag	htmlTag

" Highlights - CSS --------------------------------------------
hi! link cssBraces	Delimiter
hi! link cssSelectorOp	cssBraces
hi! link cssClassName	Normal

" Highlights - Markdown ---------------------------------------
hi! link mkdListItem	mkdDelimiter

" Highlights - JavaScript -------------------------------------
hi! link javaScriptNull	Constant
hi! link javaScriptBraces	Normal

" Highlights - Help -------------------------------------------
hi! link helpExample	String
hi! link helpHeadline	Title
hi! link helpSectionDelim	Comment
hi! link helpHyperTextEntry	Statement
hi! link helpHyperTextJump	Underlined
hi! link helpURL	Underlined
" Vim color file
" Converted from Textmate theme Brogrammer using Coloration v0.4.0 (http://github.com/sickill/coloration)

set background=dark
highlight clear

if exists("syntax_on")
  syntax reset
endif

let g:colors_name = "brogrammer"

" color             term    gui
"
" white            = 15,  #f7f3ff
" black            = 16,  #000000
" almostWhite      = 231, #ecf0f1
" almostBlack      = 234, #1a1a1a
" darkGrey         = 238, #444444
" lightGrey        = 244, #838586
" middleLightGrey  = 236, #2f2f2f
" middleDarkGrey   = 241, #606060
" middleDarkerGrey = 240, #575858
"
" primaryGreen     = 2,   #009900
" green            = 40,  #00df00
" lightGreen       = 41,  #2ecc71
" darkGreen        = 22,  #30312a
" middleDarkGreen  = 64,  #44800a
"
" blue             = 39,  #6c71c4
" darkBlue         = 23,  #1d3251
"
" purple           = 62,  #6c71c4
" darkPurple       = 68,  #3498db
"
" lightPink        = 224, #f7e8e2
" darkPink         = 88,  #880505
" middleLightPink  = 167, #e74c3c
"
" primaryRed       = 1,   #ff2222
" red              = 196, #e14d42
"
" orange           = 172, #e67e22
"
" yellow           = 220, #f1c40f

" Basic vim elements
hi Cursor ctermfg=234 ctermbg=231 cterm=NONE guifg=#1a1a1a guibg=#ecf0f1 gui=NONE
hi Visual ctermfg=NONE ctermbg=238 cterm=NONE guifg=NONE guibg=#444444 gui=NONE
hi CursorLine ctermfg=NONE ctermbg=236 cterm=NONE guifg=NONE guibg=#2f2f2f gui=NONE
hi CursorColumn ctermfg=NONE ctermbg=236 cterm=NONE guifg=NONE guibg=#2f2f2f gui=NONE
hi ColorColumn ctermfg=NONE ctermbg=236 cterm=NONE guifg=NONE guibg=#2f2f2f gui=NONE
hi LineNr ctermfg=244 ctermbg=236 cterm=NONE guifg=#838586 guibg=#2f2f2f gui=NONE
hi VertSplit ctermfg=240 ctermbg=240 cterm=NONE guifg=#575858 guibg=#575858 gui=NONE
hi MatchParen ctermfg=167 ctermbg=NONE cterm=underline guifg=#e74c3c guibg=NONE gui=underline
hi StatusLine ctermfg=231 ctermbg=240 cterm=bold guifg=#ecf0f1 guibg=#575858 gui=bold
hi StatusLineNC ctermfg=231 ctermbg=240 cterm=NONE guifg=#ecf0f1 guibg=#575858 gui=NONE
hi Pmenu ctermfg=41 ctermbg=NONE cterm=NONE guifg=#2ecc71 guibg=NONE gui=NONE
hi PmenuSel ctermfg=NONE ctermbg=238 cterm=NONE guifg=NONE guibg=#444444 gui=NONE
hi Search term=reverse cterm=bold ctermfg=234 ctermbg=220 gui=bold guifg=#1a1a1a guibg=#f1c40f
hi IncSearch term=reverse cterm=bold ctermfg=16 ctermbg=172 gui=bold guifg=#000000 guibg=#e67e22
hi Directory ctermfg=62 ctermbg=NONE cterm=NONE guifg=#6c71c4 guibg=NONE gui=NONE
hi Folded ctermfg=241 ctermbg=234 cterm=NONE guifg=#606060 guibg=#1a1a1a gui=NONE

" syntax stuff
hi Normal ctermfg=231 ctermbg=234 cterm=NONE guifg=#ecf0f1 guibg=#1a1a1a gui=NONE
hi Boolean ctermfg=62 ctermbg=NONE cterm=NONE guifg=#6c71c4 guibg=NONE gui=NONE
hi Character ctermfg=62 ctermbg=NONE cterm=NONE guifg=#6c71c4 guibg=NONE gui=NONE
hi Comment ctermfg=241 ctermbg=NONE cterm=NONE guifg=#606060 guibg=NONE gui=italic
hi Conditional ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
hi Constant ctermfg=62 ctermbg=NONE cterm=NONE guifg=#6c71c4 guibg=NONE gui=NONE
hi Define ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
hi DiffAdd ctermfg=231 ctermbg=64 cterm=bold guifg=#ecf0f1 guibg=#44800a gui=bold
hi DiffDelete ctermfg=88 ctermbg=NONE cterm=NONE guifg=#880505 guibg=NONE gui=NONE
hi DiffChange ctermfg=231 ctermbg=23 cterm=NONE guifg=#ecf0f1 guibg=#1d3251 gui=NONE
hi DiffText ctermfg=231 ctermbg=40 cterm=bold guifg=#ecf0f1 guibg=#00df00 gui=bold
hi ErrorMsg ctermfg=15 ctermbg=167 cterm=NONE guifg=#ffffff guibg=#e74c3c gui=NONE
hi WarningMsg ctermfg=15 ctermbg=167 cterm=NONE guifg=#ffffff guibg=#e74c3c gui=NONE
hi Float ctermfg=62 ctermbg=NONE cterm=NONE guifg=#6c71c4 guibg=NONE gui=NONE
hi Function ctermfg=41 ctermbg=NONE cterm=NONE guifg=#2ecc71 guibg=NONE gui=NONE
hi Identifier ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
hi Keyword ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
hi Label ctermfg=220 ctermbg=NONE cterm=NONE guifg=#f1c40f guibg=NONE gui=NONE
hi NonText ctermfg=22 ctermbg=234 cterm=NONE guifg=#30312a guibg=#1a1a1a gui=NONE
hi Number ctermfg=62 ctermbg=NONE cterm=NONE guifg=#6c71c4 guibg=NONE gui=NONE
hi Operator ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
hi PreProc ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
hi Special ctermfg=62 ctermbg=NONE cterm=NONE guifg=#6c71c4 guibg=NONE gui=NONE
hi SpecialKey ctermfg=22 ctermbg=236 cterm=NONE guifg=#f1530f guibg=#1a1a1a gui=NONE
hi Statement ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
hi StorageClass ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
hi String ctermfg=220 ctermbg=NONE cterm=NONE guifg=#f1c40f guibg=NONE gui=NONE
hi Structure ctermfg=68 ctermbg=NONE cterm=bold guifg=#3498db guibg=NONE gui=bold
hi Tag ctermfg=41 ctermbg=NONE cterm=NONE guifg=#2ecc71 guibg=NONE gui=NONE
hi Title ctermfg=231 ctermbg=NONE cterm=bold guifg=#ecf0f1 guibg=NONE gui=bold
hi Todo ctermfg=241 ctermbg=NONE cterm=inverse,bold guifg=#606060 guibg=NONE gui=inverse,bold,italic
hi Type ctermfg=41 ctermbg=NONE cterm=NONE guifg=#2ecc71 guibg=NONE gui=NONE
hi Underlined ctermfg=NONE ctermbg=NONE cterm=underline guifg=NONE guibg=NONE gui=underline
"hi SpellBad term=reverse ctermfg=167 ctermbg=224 gui=undercurl guisp=Red
hi SpellBad ctermfg=167 ctermbg=NONE cterm=bold,italic

" Ruby
hi rubyClass ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
hi rubyFunction ctermfg=41 ctermbg=NONE cterm=NONE guifg=#2ecc71 guibg=NONE gui=NONE
hi rubyInterpolationDelimiter ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi rubySymbol ctermfg=62 ctermbg=NONE cterm=NONE guifg=#6c71c4 guibg=NONE gui=NONE
hi rubyConstant ctermfg=68 ctermbg=NONE cterm=bold guifg=#3498db guibg=NONE gui=bold
hi rubyStringDelimiter ctermfg=220 ctermbg=NONE cterm=NONE guifg=#f1c40f guibg=NONE gui=NONE
hi rubyBlockParameter ctermfg=172 ctermbg=NONE cterm=NONE guifg=#e67e22 guibg=NONE gui=NONE
hi rubyInstanceVariable ctermfg=172 ctermbg=NONE cterm=NONE guifg=#e67e22 guibg=NONE gui=NONE
hi rubyInclude ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
hi rubyGlobalVariable ctermfg=172 ctermbg=NONE cterm=NONE guifg=#e67e22 guibg=NONE gui=NONE
hi rubyRegexp ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
hi rubyRegexpDelimiter ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
hi rubyEscape ctermfg=62 ctermbg=NONE cterm=NONE guifg=#6c71c4 guibg=NONE gui=NONE
hi rubyControl ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
hi rubyClassVariable ctermfg=172 ctermbg=NONE cterm=NONE guifg=#e67e22 guibg=NONE gui=NONE
hi rubyOperator ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
hi rubyException ctermfg=167 ctermbg=NONE cterm=bold guifg=#e74c3c guibg=NONE gui=bold
hi rubyPseudoVariable ctermfg=172 ctermbg=NONE cterm=NONE guifg=#e67e22 guibg=NONE gui=NONE
hi rubyRailsUserClass ctermfg=68 ctermbg=NONE cterm=bold guifg=#3498db guibg=NONE gui=bold
hi rubyRailsARAssociationMethod ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
hi rubyRailsARMethod ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
hi rubyRailsRenderMethod ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
hi rubyRailsMethod ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
hi erubyDelimiter ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi erubyComment ctermfg=241 ctermbg=NONE cterm=NONE guifg=#606060 guibg=NONE gui=italic
hi erubyRailsMethod ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE

" Frontend stuff
hi htmlTag ctermfg=167 ctermbg=NONE cterm=NONE guifg=#e74c3c guibg=NONE gui=NONE
hi htmlEndTag ctermfg=167 ctermbg=NONE cterm=NONE guifg=#e74c3c guibg=NONE gui=NONE
hi htmlTagName ctermfg=167 ctermbg=NONE cterm=NONE guifg=#e74c3c guibg=NONE gui=NONE
hi htmlArg ctermfg=167 ctermbg=NONE cterm=NONE guifg=#e74c3c guibg=NONE gui=NONE
hi htmlSpecialChar ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
hi javaScriptFunction ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
hi javaScriptRailsFunction ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
hi javaScriptBraces ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi yamlKey ctermfg=41 ctermbg=NONE cterm=NONE guifg=#2ecc71 guibg=NONE gui=NONE
hi yamlAnchor ctermfg=172 ctermbg=NONE cterm=NONE guifg=#e67e22 guibg=NONE gui=NONE
hi yamlAlias ctermfg=172 ctermbg=NONE cterm=NONE guifg=#e67e22 guibg=NONE gui=NONE
hi yamlDocumentHeader ctermfg=220 ctermbg=NONE cterm=NONE guifg=#f1c40f guibg=NONE gui=NONE
hi cssURL ctermfg=172 ctermbg=NONE cterm=NONE guifg=#e67e22 guibg=NONE gui=NONE
hi cssFunctionName ctermfg=68 ctermbg=NONE cterm=NONE guifg=#3498db guibg=NONE gui=NONE
hi cssColor ctermfg=62 ctermbg=NONE cterm=NONE guifg=#6c71c4 guibg=NONE gui=NONE
hi cssPseudoClassId ctermfg=41 ctermbg=NONE cterm=NONE guifg=#2ecc71 guibg=NONE gui=NONE
hi cssClassName ctermfg=41 ctermbg=NONE cterm=NONE guifg=#2ecc71 guibg=NONE gui=NONE
hi cssValueLength ctermfg=62 ctermbg=NONE cterm=NONE guifg=#6c71c4 guibg=NONE gui=NONE
hi cssCommonAttr ctermfg=41 ctermbg=NONE cterm=NONE guifg=#2ecc71 guibg=NONE gui=NONE
hi cssBraces ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE

" GitGutter Customizations
hi SignColumn ctermfg=244 ctermbg=236 guifg=#838586 guibg=#2f2f2f
hi GitGutterChangeDefault ctermfg=244 ctermbg=236 guifg=#bbbb00 guibg=#2f2f2f
hi GitGutterAddDefault ctermfg=2 ctermbg=236 guifg=#009900 guibg=#2f2f2f
hi GitGutterDeleteDefault ctermfg=1 ctermbg=236 guifg=#ff2222 guibg=#2f2f2f
