# My personal Vim setup on Arch

[https://gitlab.com/bmansfield/dotvim](https://gitlab.com/bmansfield/dotvim)

![My VIM setup screenshot](screenshot.jpg)

Screenshot - Arch Linux + alacritty + TMUX + VIM 8


## About

My VIM configuration. I use it to quickly pull changes for my other computers.
If you find it a good starting place for yourself, fork it, clone it, take what
you like, change what you don't.  If there are corrections or broken things feel
free to send me a PR, otherwise just fork it and keep it for your own. These are
instructions specifically for my Arch Linux setup. See the master branch for Mac
specific configs.


## Featured Plug-in Highlights

* [Pathogen](https://github.com/tpope/vim-pathogen) - for easily managing plugins as git submodules
* [NerdTree](https://github.com/scrooloose/nerdtree) - nice tree-view file browsing [C-n]
* [Syntastic](https://github.com/scrooloose/syntastic) - syntax checking and correction helpers for most major languages
* [Airline](https://github.com/bling/vim-airline) - Powerful Vim status bar light as air
* [Rainbow Parentheses](https://github.com/kien/rainbow_parentheses.vim) - easily see when and where you are missing a closing bracket

Just to name a few...

See the submodules for a full list


## Featured Personal Customizations

* Spell checking customizations
* Search customizations
* Custom text wrapping
* Hack for multiple pastes
* Fixes for italics
* Kills unneeded white spaces
* Repurposed arrow keys (because we should be staying on home row)
* Color theme settings
* Ctags set-up
* Custom settings for specific coding
* Custom configs for the list of plug-ins
* Custom functions to do really neat things
* A whole bunch of custom key mappings to save me key strokes for all my
  favorite vim commands

And many many more things


## Usage / Installation

### General

This should get you all the basics to get started

1. Clone .vim directory

```shell
git clone https://gitlab.com/bmansfield/dotvim.git ~/.vim
```

2. Install submodules

```shell
cd ~/.vim
git submodule update --init --recursive
```

3. Symlink vimrc

```shell
ln -s ~/.vim/vimrc ~/.vimrc
```


### Other

Until I get around to writing a bash script to do all this automagically for
me. There will be a few extra things you will need to install in order for this
full list of vim goodies to work.


#### Install YouCompleteMe

To completely install YouCompleteMe you need to do a few extra steps. I suggest
using their repo installation instructions in case something has changed.
Since I'm usually setting this up for myself, and I write a lot of Golang. My
typical installation looks something like this

Update the submodules

```shell
git submodule update --init --recursive
```

Then use their installer script with C and Go flags. Don't forget to check their
official install instructions in case something changed.

```shell
cd ~/.vim/bundle/YouCompleteMe
python3 ./install.py --go-completer
```


#### Airline Support

In order to get the full on Airline support, you'll need to install the missing
[Menlo Powerline patched fonts](https://github.com/abertsch/Menlo-for-Powerline).

```shell
git clone https://github.com/abertsch/Menlo-for-Powerline.git
```
* Double click the fonts to install them.


#### CTags/Tagbar

In order for tagbar to work properly with Golang, you will want to install
gotags.

```bash
go get -u github.com/jstemmer/gotags
```


### Updating

If you just want to do an update of submodules, you can run

```bash
git submodule update --recursive --remote
```

Then you will want to `git add bundle/<submodule>`, commit and push. If you
update `You` you usually need to go and re-run the install script. See above for
details on how to do that.


## Notes

Use at your own risk. You may be eaten by a grue.

Other things to note. Using on Arch Linux with AwesomeWM in alacritty + tmux can
yield all kinds of weird things you will have to troubleshoot, such as copy and
paste issues, italic font support, color theme settings, and many more fun things.
I will try to annotate this more as I come across it. Some of this support overlaps
to my [dotfile repo](https://gitlab.com/bmansfield/dotfiles). You may want to
check there for some of these issues, tmux in particular.

